//
//  InformationsViewController.m
//  XeeTourizm
//
//  Created by Morgan Collino on 06/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "InformationsViewController.h"

@interface InformationsViewController ()

@end

@implementation InformationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;

    self.title = @"Informations";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
