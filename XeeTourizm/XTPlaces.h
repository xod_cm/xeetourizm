//
//  XTPlaces.h
//  XeeTourizm
//
//  Created by Morgan Collino on 01/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface XTPlaces : NSObject
{
    CLLocationCoordinate2D location;
    NSString *name;
    NSString *streetName;
    NSString *city;
    NSString *description;
    NSString *categories;
    NSString *country;
    NSString *state;
    
    double   distance;
}

- (id) initWithDictionary: (NSDictionary *) d;

@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSString *city;
@property (nonatomic, strong, readonly) NSString *streetName;
@property (nonatomic, strong, readonly) NSString *country;
@property (nonatomic, strong, readonly) NSString *state;
@property (nonatomic, strong, readonly) NSString *description;
@property (nonatomic, strong, readonly) NSString *categories;
@property (nonatomic, readonly) CLLocationCoordinate2D location;
@property (nonatomic, readwrite) double distance;

@end
