//
//  XTWarning.h
//  XeeTourizm
//
//  Created by Morgan Collino on 03/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XTWarning : NSObject
{
    NSString *warningItem;
    NSString *warningCause;
    
    NSString *warningCodeForGoogleSearch;
    NSString *warningCodeFor4SquareSearch;
}
@end
