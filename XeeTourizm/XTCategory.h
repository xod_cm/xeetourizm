//
//  XTCategory.h
//  XeeTourizm
//
//  Created by Morgan Collino on 08/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XTCategory : NSObject

/*
 Sorties (amusement_park, art_gallery, bar, bowling_alley, cafe, casino, food, meal_takeaway, movie_theater, museum, night_club, park, restaurant, zoo)
 */

/*
 Store (clothing_store, florist, furniture_store, grocery_or_supermarket, home_goods_store, liquor_store, shoe_store, shopping_mall)
 */

/*
 Transport (airport, bus_station, parking, subway_station, taxi_stand, train_station, car_wash, gas_station)
 */

/*
 Transport utils (car_dealer, car_rental, car_repair, bicycle_store, car_wash, gas_station)
 */

/*
 Utils (atm, bank, city_hall, courthouse, electrician, embassy, fire_station, hospital, police, post_office, stadium)
 */

/*
 Education (book_store, library, school, university)
 */


/*
 Beauty (beauty_salon, spa, hair_care)
 */

/*
 Religious places (church, cemetery, hindu_temple, mosque, synagogue)
 
 */

/*
 Health (dentist, doctor, gym, health, pharmacy, veterinary_care)
 */

// [Sorties, Magasins, Transport,


+ (NSArray *)categories;
+ (NSDictionary *)subCategoriesForCategory: (NSString *) category;



@end
