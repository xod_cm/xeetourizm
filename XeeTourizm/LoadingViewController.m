//
//  LoadingViewController.m
//  XeeTourizm
//
//  Created by Morgan Collino on 06/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "LoadingViewController.h"
#import "XeeManager.h"
#import "XEE.h"
#import "RootViewController.h"

@interface LoadingViewController ()

@end

@implementation LoadingViewController

@synthesize label = _label, loader = _loader, appName = _appName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;

    @try {
        XeeManager *manager = [XeeManager sharedInstance];
        [manager setDelegate:  self];
        XEEController *xee = [XEEController sharedInstance];
        NSURL *myRedirectionUri = [NSURL URLWithString:@"http://localhost"];
        XEEClient *myClient = [[XEEClient alloc] initWithIdentifier:@"egZuGntLpAdiVwMX0E3U" password:@"cnPVmKeED925d0kRKe3b" redirectUri: myRedirectionUri scopes: @[@"user", @"data"] name:@"DevClient80"];
        xee.client = myClient;
        
        xee.delegate = manager;
        xee.boxSerialNumber = @"5012SN0040";
        xee.useDevelopmentEnvironment = YES;
        [xee setUseSimulationMode:YES];
        [xee setClient:myClient];
        [xee authenticateUser];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
    }
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) didAuthenticate
{
    [_loader stopAnimating];
    _loader.hidden = YES;
    
    RootViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier: @"rootViewController"];
    [self.navigationController pushViewController: controller animated: YES];
}

@end
