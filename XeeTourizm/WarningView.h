//
//  WarningView.h
//  XeeTourizm
//
//  Created by Morgan Collino on 03/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface WarningView : UIView
{
    UIButton *button;
    UIButton *close;

    UIImageView *image;
    UILabel *label;
    NSString *textQuestion;
    NSString *tokenSearch;
    
    
    RootViewController *delegate;
    
}

@property (nonatomic, retain) IBOutlet UIButton *button;
@property (nonatomic, retain) IBOutlet UIButton *close;
@property (nonatomic, retain) IBOutlet UIImageView *image;
@property (nonatomic, retain) IBOutlet UILabel *label;
@property (nonatomic) NSString *textQuestion;
@property (nonatomic) NSString *tokenSearch;

@property (nonatomic, weak) RootViewController *delegate;

- (void) setText: (NSString *) text;

@end
