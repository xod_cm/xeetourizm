//
//  CategoryCollectionReusableView.h
//  XeeTourizm
//
//  Created by Morgan Collino on 11/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCollectionReusableView : UICollectionReusableView
{
    UIImageView *image;
    UILabel *label;
}

@property (nonatomic, retain) IBOutlet UIImageView *image;
@property (nonatomic, retain) IBOutlet UILabel *label;

@end
