//
//  PlaceTableViewController.h
//  XeeTourizm
//
//  Created by Morgan Collino on 04/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceTableViewController : UITableViewController
{
    NSArray *arrayPlaces;
}

@property (nonatomic, retain) NSArray *arrayPlaces;

@end
