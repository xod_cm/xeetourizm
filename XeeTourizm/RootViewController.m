//
//  RootViewController.m
//  XeeTourizm
//
//  Created by Morgan Collino on 26/08/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "RootViewController.h"
#import "XeeManager.h"
#import "XEE.h"
#import "XT4SquareAPIPlaceHandler.h"
#import "XTGoogleAPIPlaceHandler.h"
#import "PlaceViewController.h"
#import "WarningView.h"
#import "XTPlaces.h"
#import "XTUtils.h"
#import "RootAppDelegate.h"
#import "XTTellMeWherePlaceHandler.h"
#import "InformationsViewController.h"
#import "VehicleStateController.h"
#import "XTCategory.h"
#import "CategoriesTableViewController.h"
#import "CategoryCollectionViewCell.h"

@interface RootViewController ()
{
    NSMutableArray *categories;
    NSMutableArray *categoriesImages;
}
@end

@implementation RootViewController

@synthesize wView = _wView, container = _container, test = _test, infos = _infos, fuel = _fuel, speed = _speed, etatVehicule = _etatVehicule, collectionView = _collectionView;

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear: animated];
    [[XeeManager sharedInstance] setDelegate: self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    categories = [[NSMutableArray alloc] initWithArray:[XTCategory categories]];
    [categories addObject: @"Etat véhicule"];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];

  /*
    [array addObject: @"Sorties"];
    [array addObject: @"Magasins"];
    [array addObject: @"Transport"];
    [array addObject: @"Utilitaires"];
    [array addObject: @"Education"];
    [array addObject: @"Religion"];
    [array addObject: @"Beauté"];
    [array addObject: @"Santé"];*/
    

    
    categoriesImages = [[NSMutableArray alloc] init];
    [categoriesImages addObject: @"bar.png"];
    [categoriesImages addObject: @"store.png"];
    [categoriesImages addObject: @"bus.png"];
    [categoriesImages addObject: @"gear.png"];
    [categoriesImages addObject: @"school.png"];
    [categoriesImages addObject: @"mind.png"];
    [categoriesImages addObject: @"beauty.png"];
    [categoriesImages addObject: @"hosto.png"];
    [categoriesImages addObject: @"car.png"];
   // [[XeeManager sharedInstance] setDelegate: self];
    
    
    [_wView setHidden:YES];
    [_wView setDelegate: self];
    
    /*
    [XT4SquareAPIPlaceHandler searchPlace:location forQuery: @"station" limit:10 onSucces:^(NSArray *response)
    {
        NSLog(@"Response: %@", response);
        
    } onError:^(NSError *error) {
        
        NSLog(@"Error: %@", error.description);
        
    }];
    */
    
   /* [XTGoogleAPIPlaceHandler searchPlace:location forTypes: @[@"gas_station"] limit:-42 onSucces:^(NSArray *response)
     {
         NSLog(@"Response: %@", response);
         NSMutableArray *tmp = [[NSMutableArray alloc] init];
         for (XTPlaces *t in response)
         {
             if (t.distance == 0.0)
             {
                 t.distance = [XTUtils distanceFromFirstCoordinate:location toSecondCoordinate:t.location];
             }
             [tmp addObject: t];
         }
         _test = tmp;
        
         
         
         
         //[_wView setHidden:NO];
         
         
         [UIView animateWithDuration:0.0
                               delay: 0.0
                             options: UIViewAnimationOptionCurveEaseOut
                          animations:^{
                              _wView.alpha = 0.0;
                              _wView.hidden = NO;
                          }
                          completion:^(BOOL finished){
                              // Wait one second and then fade in the view
                              [UIView animateWithDuration:0.5
                                                    delay: 1.0
                                                  options:UIViewAnimationOptionCurveEaseIn
                                               animations:^{
                                                   _wView.alpha = 1.0;
                                               }
                                               completion:nil];
                          }];
         
     } onError:^(NSError *error) {
         
         NSLog(@"Error: %@", error.description);
         
     }];
    */
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showInformations:(id)sender
{
    InformationsViewController *infos = [self.storyboard instantiateViewControllerWithIdentifier:@"informationsViewController"];
    [[self navigationController] pushViewController:infos animated: YES];
}


- (IBAction)showEtatVehicule:(id)sender
{
    VehicleStateController *infos = [self.storyboard instantiateViewControllerWithIdentifier:@"vehicleStateController"];
    infos.title = @"État véhicule";
    [[self navigationController] pushViewController:infos animated: YES];
}



- (void) didReceiveNewVehiculeSpeed:(double)value
{
    NSLog(@"Vehicule speed: %f", value);
    [_speed setText: [NSString stringWithFormat: @"Speed: %.2f", value]];
}

- (void) didReceiveNewFuelLevel:(double)value
{
    NSLog(@"Fuel level: %f", value);
    [_fuel setText: [NSString stringWithFormat: @"Fuel Level: %f", value]];

}


- (void) didReceiveNewLocation:(CLLocationCoordinate2D)location
{
    NSLog(@"New Location: {%f, %f}", location.latitude, location.longitude);
}

- (void)pushCategory:(NSString *) category
{
    NSArray *keys = [[XTCategory subCategoriesForCategory: category] allKeys];
    NSLog(@"Keys: %@", keys);

    NSArray *subcategories = [[NSArray alloc] initWithArray: [[[XTCategory subCategoriesForCategory: category] objectForKey: [keys objectAtIndex: 0]] allKeys]];
    NSLog(@"Categories: %@", subcategories);

    NSDictionary *categoriesWithKey = [[NSDictionary alloc] initWithDictionary: [XTCategory subCategoriesForCategory: category]];
    NSLog(@"categoriesWithKey: %@", categoriesWithKey);

    
    CategoriesTableViewController *categoriesTable = [self.storyboard instantiateViewControllerWithIdentifier:@"categoriesTableViewController"];
    
    categoriesTable.categories = subcategories;
    categoriesTable.categoriesWithKey = categoriesWithKey;
    categoriesTable.title = category;
    
    [[self navigationController] pushViewController:categoriesTable animated: YES];
}



#pragma UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *label = [categories objectAtIndex: indexPath.row];
    
    if (!(indexPath.row == [categories count] - 1))
        [self pushCategory: label];
    else
        [self showEtatVehicule: nil];
}

#pragma UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [categories count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
    
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryCollectionViewCell *categoryCell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"categoryCollectionViewCell" forIndexPath:indexPath];
    
    NSString *label = [categories objectAtIndex: indexPath.row];
    NSString *imageName = [categoriesImages objectAtIndex: indexPath.row];
    if (imageName.length > 0)
    {
        [categoryCell.image setImage: [UIImage imageNamed: imageName]];
    }
    
    categoryCell.label.text = label;
    
    return categoryCell;
}

- (void) alertRunOutGas
{
    if (![_wView isHidden])
        return;
    [_wView setHidden:NO];
    [[_wView label] setText: @"Niveau d'essence bas !"];
    [_wView setTextQuestion: @"Rechercher les stations services proches ?"];
    [_wView setTokenSearch: @"petrol-stations"];
    [UIView animateWithDuration:0.0
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _wView.alpha = 0.0;
                         _wView.hidden = NO;
                     }
                     completion:^(BOOL finished){
                         // Wait one second and then fade in the view
                         [UIView animateWithDuration:0.5
                                               delay: 1.0
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              _wView.alpha = 1.0;
                                          }
                                          completion:nil];
                     }];
}

@end
