//
//  XTTellMeWherePlaceHandler.m
//  XeeTourizm
//
//  Created by Morgan Collino on 06/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "XTTellMeWherePlaceHandler.h"
#import "AFJSONRequestOperation.h"
#import "XTPlacesFactory.h"

@implementation XTTellMeWherePlaceHandler


+ (void) searchPlace: (CLLocationCoordinate2D) location forCategorie: (NSString *) query  limit: (int) limit onSucces: (void (^) (NSArray *response)) respondeHandler onError: (void (^) (NSError *error)) errorHandler
{
    NSString *urlAPI = [NSString stringWithFormat: @"%@?center=%f,%f&lang=fr&apikey=%@&query=%@", API_TMW_URL, location.latitude, location.longitude, API_TMW__KEY, query];
    
    NSURL *url = [[NSURL alloc] initWithString: urlAPI];
        
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSArray *venues = [XTPlacesFactory parseTMWAPIResponse: (NSDictionary *) JSON];
                                             NSLog(@"Venues: %@", venues);
                                             respondeHandler(venues);
                                         }
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            errorHandler(error);
                                                                                        }];
    
    [operation start];
}

@end
