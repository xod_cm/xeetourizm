//
//  XTPlaces.m
//  XeeTourizm
//
//  Created by Morgan Collino on 01/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "XTPlaces.h"
#import "XTTry.h"

@implementation XTPlaces

@synthesize name = _name, description = _description, city = _city, location = _location, streetName = _streetName, state = _state, categories = _categories, country = _country, distance = _distance;

- (id) initWithDictionary:(NSDictionary *)d
{
    if (self = [super init])
    {
        /*
         
            Dictionary: 
                @"city" : @"value",
                @"description" : @"value",
                @"longitude" : @"value",
                @"latitude" : @"value",
                @"type" : @"value",
                @"streetName": @"value",
                @"name" : @"name"
         */
        
        NSLog(@"Maillon: %@", d);
        
        _city = [[NSString alloc] initWithString: [XTTry tryGettingString: d forKey: @"city" defaultValue: @"Unknown city"]];
        _description = [[NSString alloc] initWithString: [XTTry tryGettingString: d forKey: @"description" defaultValue: @""]];
        _categories = [[NSString alloc] initWithString: [XTTry tryGettingString: d forKey: @"categories" defaultValue: @"Unknown categories"]];
        _streetName = [[NSString alloc] initWithString: [XTTry tryGettingString: d forKey: @"streetName" defaultValue: @"Unknown street name"]];
        _name = [[NSString alloc] initWithString: [XTTry tryGettingString: d forKey: @"name" defaultValue: @"Unknown name"]];
        _country = [[NSString alloc] initWithString: [XTTry tryGettingString: d forKey: @"country" defaultValue: @"Unknown country"]];
        _state  = [[NSString alloc] initWithString: [XTTry tryGettingString: d forKey: @"state" defaultValue: @"Unknown state"]];
        _location = CLLocationCoordinate2DMake([[XTTry tryGettingNumber:d forKey:@"latitude"] doubleValue], [[XTTry tryGettingNumber:d forKey:@"longitude"] doubleValue]);
        _distance = [[XTTry tryGettingNumber:d forKey:@"distance"] doubleValue];
    }
    
    return self;
}

@end
