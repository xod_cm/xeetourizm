//
//  PlaceCell.h
//  XeeTourizm
//
//  Created by Morgan Collino on 04/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceCell : UITableViewCell
{
    UILabel *name;
    UILabel *city;
    UILabel *streetName;
    UILabel *distance;
}

@property (nonatomic, retain) IBOutlet UILabel *name;
@property (nonatomic, retain) IBOutlet UILabel *city;
@property (nonatomic, retain) IBOutlet UILabel *streetName;
@property (nonatomic, retain) IBOutlet UILabel *distance;

@end
