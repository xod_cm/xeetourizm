//
//  XeeManager.m
//  XeeTourizm
//
//  Created by Morgan Collino on 26/08/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "XeeManager.h"


@implementation XeeManager

@synthesize delegate = _delegate, location = _location, locationManager = _locationManager, isConnect = _isConnect;

static XeeManager *instance = nil;

- (instancetype) init
{
    if (self = [super init])
    {
        _location = CLLocationCoordinate2DMake(48.853, 2.35);
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        [_locationManager startUpdatingLocation];
    }
    return self;
}

+ (XeeManager *) sharedInstance
{
    static dispatch_once_t pred;        
    dispatch_once(&pred, ^{
        instance = [[XeeManager alloc] init];
    });
    
    return instance;
}

- (void)XEEControllerDidAuthenticateUser
{
    // The user has been successfully authenticated using Xee's OAuth servers
    NSLog(@"XEEControllerDidAuthenticateUser");
    
    XEEController *xee = [XEEController sharedInstance];
    //xee.boxSerialNumber = @"5012SN0040";
    xee.useSimulationMode = YES;
    [xee connectToDevice];

    DQUtils *utils = [[DQUtils alloc] init];
    NSDictionary *name = [utils namesForIds];
    NSLog(@"Names: %@", name);
   
}
- (void)XEEControllerDidFailAuthenticatingUserWithError:(NSError *)error
{
    // An error occured during the authentication process (the user canceled it, the authentication servers were not available…)
    NSLog(@"XEEControllerDidFailAuthenticatingUserWithError: %@", error);

}
- (void)XEEControllerDidConectToXEEDevice
{
    // The connection with the device is active. New data will start arriving
    NSLog(@"XEEControllerDidConnectToXEEDevice");
    _isConnect = YES;
    if (_delegate && [_delegate respondsToSelector: @selector(didAuthenticate)])
        [_delegate didAuthenticate];

}
- (void)XEEControllerDidDisconnectFromXEEDevice
{
    // The connection with the device has been closed
    NSLog(@"XEEControllerDidDisconnectFromXEEDevice");
    _isConnect = NO;

}
- (void)XEEControllerDidFailConnectingToXEEDeviceWithError:(NSError *)error
{
    // An error occured while connecting with the device
    NSLog(@"XEEControllerDidFailConnectingToXEEDeviceWithError: %@", error);
}
- (void)XEEDeviceDidReceiveNewDataForSignals:(NSArray *)signals
{
    // New data available
    //NSLog(@"XEEDeviceDidReceiveNewDataForSignals: %@", signals);
    for (NSNumber *i in signals)
    {
    }
    
    XEEDataService *service = [[XEEController sharedInstance] dataService];
    //NSLog(@"Last datas: %@", [service lastAvailableData]);
    
    NSDictionary *datas = [service lastAvailableData];
    
    @try
    {
    
        /*if ([datas objectForKey: @"location"] != nil)
        {
            NSDictionary *locationDictionary = [datas objectForKey: @"location"];
            double longitude = [[locationDictionary objectForKey: @"longitude"] doubleValue];
            double latitude = [[locationDictionary objectForKey: @"latitude"] doubleValue];
            CLLocationCoordinate2D location = CLLocationCoordinate2DMake(longitude, latitude);
            [_delegate didReceiveNewLocation:location];
        }
        */
        if ([datas objectForKey: [[DQUtils sharedController] SignalNameVehiculeSpeed]] != nil && _delegate)
        {
            DQData *data = [datas objectForKey: [[DQUtils sharedController] SignalNameVehiculeSpeed]];
            if ([_delegate respondsToSelector: @selector(didReceiveNewVehiculeSpeed:)])
            [_delegate didReceiveNewVehiculeSpeed: data.value];
        }

        
        if ([datas objectForKey: [[DQUtils sharedController] SignalNameFuelLevel]] != nil && _delegate)
        {
            DQData *data = [datas objectForKey: [[DQUtils sharedController] SignalNameFuelLevel]];
            if ([_delegate respondsToSelector: @selector(didReceiveNewVehiculeSpeed:)])
            [_delegate didReceiveNewVehiculeSpeed: data.value];
        }
        
        if ([datas objectForKey: [[DQUtils sharedController] SignalNameBrakePedalPosition]] != nil && _delegate)
        {
            DQData *data = [datas objectForKey: [[DQUtils sharedController] SignalNameBrakePedalPosition]];
            if ([_delegate respondsToSelector: @selector(didReceiveNewBrakePosition:)])
            [_delegate didReceiveNewBrakePosition: data.value];
        }
        
        if ([datas objectForKey: [[DQUtils sharedController] SignalNameThrottlePedalPosition]] != nil && _delegate)
        {
            DQData *data = [datas objectForKey: [[DQUtils sharedController] SignalNameThrottlePedalPosition]];
            if ([_delegate respondsToSelector: @selector(didReceiveNewThrottlePosition:)])
            [_delegate didReceiveNewThrottlePosition: data.value];
        }
        
        if ([datas objectForKey: [[DQUtils sharedController] SignalNameEngineSpeed]] != nil && _delegate)
        {
            DQData *data = [datas objectForKey: [[DQUtils sharedController] SignalNameEngineSpeed]];
            if ([_delegate respondsToSelector: @selector(didReceiveNewRPM:)])
            [_delegate didReceiveNewRPM: data.value];
        }
        
        if ([datas objectForKey: [[DQUtils sharedController] SignalNameBatteryVoltage]] != nil && _delegate)
        {
            DQData *data = [datas objectForKey: [[DQUtils sharedController] SignalNameBatteryVoltage]];
            if ([_delegate respondsToSelector: @selector(didReceiveNewVoltage:)])
            [_delegate didReceiveNewVoltage: data.value];
        }
        
        if ([datas objectForKey: [[DQUtils sharedController] SignalNameFuelLevel]] != nil && _delegate)
        {
            DQData *data = [datas objectForKey: [[DQUtils sharedController] SignalNameFuelLevel]];
            if (data.value < LIMIT_GAS && data.value > 0.0 && [_delegate respondsToSelector: @selector(alertRunOutGas)])
                [_delegate alertRunOutGas];
        }
        
        if ([datas objectForKey: [[DQUtils sharedController] SignalNameLockSts]] != nil && _delegate)
        {
            DQData *data = [datas objectForKey: [[DQUtils sharedController] SignalNameLockSts]];
            if ([_delegate respondsToSelector: @selector(didReceiveNewLockSts:)])
                [_delegate didReceiveNewLockSts: data.value];
        }
        
        if ([datas objectForKey: [[DQUtils sharedController] SignalNameBrakePedalSts]] != nil && _delegate)
        {
            DQData *data = [datas objectForKey: [[DQUtils sharedController] SignalNameBrakePedalSts]];
            if ([_delegate respondsToSelector: @selector(didReceiveNewBrakePedalSts:)])
                [_delegate didReceiveNewBrakePedalSts: data.value];
        }

        
        if ([datas objectForKey: [[DQUtils sharedController] SignalNameThrottlePedalSts]] != nil && _delegate)
        {
            DQData *data = [datas objectForKey: [[DQUtils sharedController] SignalNameThrottlePedalSts]];
            if ([_delegate respondsToSelector: @selector(didReceiveNewThrottleSts:)])
                [_delegate didReceiveNewThrottleSts: data.value];
        }

        
        /*
         
         - (void) didReceiveNewBrakePedalSts: (double) value;
         - (void) didReceiveNewThrottleSts: (double) value;
         */
        
    }
    @catch (NSException *exception)
    {
       // NSLog(@"Exception: %@", exception.reason);
    }
}


#pragma CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    _location = [newLocation coordinate];
    NSLog(@"New location : {%f,%f}", _location.latitude, _location.longitude);
}


@end
