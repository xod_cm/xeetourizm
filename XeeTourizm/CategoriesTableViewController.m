//
//  CategoriesTableViewController.m
//  XeeTourizm
//
//  Created by Morgan Collino on 10/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "CategoriesTableViewController.h"
#import "XT4SquareAPIPlaceHandler.h"
#import "XTGoogleAPIPlaceHandler.h"
#import "XTTellMeWherePlaceHandler.h"
#import "XeeManager.h"
#import "XTUtils.h"
#import "XTPlaces.h"
#import "PlaceTableViewController.h"
#import "MBProgressHUD.h"

@interface CategoriesTableViewController ()

@end

@implementation CategoriesTableViewController

@synthesize categories = _categories, categoriesWithKey = _categoriesWithKey;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSString *labelText = [_categories objectAtIndex: indexPath.row];
    
    cell.textLabel.text = labelText;
    
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Selected: %@", [_categories objectAtIndex: indexPath.row]);
    
    NSString *category = [_categories objectAtIndex: indexPath.row];
    
    //google_api
    NSDictionary *d = [_categoriesWithKey objectForKey: @"google_api"];
    NSString *key = [d objectForKey: category];
    
    
    MBProgressHUD *hud  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading";
    
    [XTGoogleAPIPlaceHandler searchPlace:[[XeeManager sharedInstance] location] forTypes:@[key] limit:-42 onSucces:^(NSArray *array)
    {
    
        NSLog(@"Response: %@", array);
        NSMutableArray *tmp = [[NSMutableArray alloc] init];
        for (XTPlaces *t in array)
        {
            if (t.distance == 0.0)
            {
                t.distance = [XTUtils distanceFromFirstCoordinate: [[XeeManager sharedInstance] location] toSecondCoordinate:t.location];
            }
            [tmp addObject: t];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        
        
        PlaceTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"placeTableViewController"];
        controller.arrayPlaces = tmp;
        controller.title = category;
        [self.navigationController pushViewController: controller animated: YES];
        
        
    } onError:^(NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });

    }];
    
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
