//
//  RootViewController.h
//  XeeTourizm
//
//  Created by Morgan Collino on 26/08/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XEE.h"
#import "XeeManager.h"

@class WarningView;

@interface RootViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, XeeManagerDelegate>
{
    NSArray *test;
}

@property (nonatomic, retain)  NSArray *test;
@property(nonatomic,retain) IBOutlet WarningView *wView;
@property(nonatomic,retain) IBOutlet UIView *container;
@property(nonatomic, retain) IBOutlet UIButton *infos;
@property(nonatomic, retain) IBOutlet UIButton *etatVehicule;
@property(nonatomic, retain) IBOutlet UILabel *speed;
@property(nonatomic, retain) IBOutlet UILabel *fuel;
@property(nonatomic, retain) IBOutlet UICollectionView *collectionView;



@end
