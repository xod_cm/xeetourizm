//
//  PlaceCell.m
//  XeeTourizm
//
//  Created by Morgan Collino on 04/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "PlaceCell.h"

@implementation PlaceCell

@synthesize city = _city, distance = _distance, streetName = _streetName, name = _name;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
