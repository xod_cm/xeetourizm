//
//  PlaceViewController.m
//  XeeTourizm
//
//  Created by Morgan Collino on 02/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//


#import "PlaceViewController.h"
#import "XTPlaces.h"
#import <MapKit/MapKit.h>
#import "XeeManager.h"

@interface PlaceViewController ()
{
    UIAlertView *alertView;
}
@end

@implementation PlaceViewController

@synthesize city = _city, itinerate = _itinerate, mapView = _mapView, name = _name, streetName = _streetName, image = _image, distance = _distance, place = _place;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    alertView = [[UIAlertView alloc] initWithTitle:@"Application" message:@"Choisissez l'app qui affichera l'itineraire" delegate:self cancelButtonTitle:@"Retour" otherButtonTitles:@"Apple Maps", @"Google Maps", nil];

    
    if (_place)
    {
        self.title = @"Detail";
        if (_place.distance > 0)
            [_distance setText: [NSString stringWithFormat: @"Distance: %.2f kmètres", _place.distance / 1000]];
        else
            [_distance setText: [NSString stringWithFormat: @"Distance: %.2f kmètres", _place.distance]];
        [_streetName setText: _place.streetName];
        [_name setText: _place.name];
        [_city setText: _place.city];
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate: _place.location];
        [annotation setTitle: _place.name];
        [_mapView addAnnotation: annotation];
        [_mapView setCenterCoordinate: _place.location];
        
        MKCoordinateRegion region;
        MKCoordinateSpan span;
        
        span.latitudeDelta = 0.005;
        span.longitudeDelta = 0.005;
        region.span = span;
        region.center = _place.location;
        [_mapView setRegion: region];
        [_mapView setUserInteractionEnabled: NO];
    }
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doItineraire:(id)sender
{
    NSLog(@"Itineraire");
    [alertView show];
}

#pragma UIAlertViewDelegate


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"Index:%d", buttonIndex);
    
    // temporaire
    CLLocationCoordinate2D location = [[XeeManager sharedInstance] location];

    if (buttonIndex == 1)
    {
        NSURL *myURL = [NSURL URLWithString: [NSString stringWithFormat: @"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f", location.latitude,location.longitude, _place.location.latitude, _place.location.longitude]];
        [[UIApplication sharedApplication] openURL:myURL];
    }
    
    if (buttonIndex == 2)
    {
        NSURL *myURL = [NSURL URLWithString: [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f", location.latitude,location.longitude, _place.location.latitude, _place.location.longitude]];
        [[UIApplication sharedApplication] openURL:myURL];
    }
}

@end
