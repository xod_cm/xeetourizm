//
//  XTGoogleAPIPlaceHandler.m
//  XeeTourizm
//
//  Created by Morgan Collino on 01/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "XTGoogleAPIPlaceHandler.h"
#import "AFJSONRequestOperation.h"
#import "XTPlacesFactory.h"

@implementation XTGoogleAPIPlaceHandler

// List of types (NSArray *) available in XTGoogleAPIPlaceHandler.h (res:  type1|type2|type3 )

+ (void) searchPlace: (CLLocationCoordinate2D) location forTypes: (NSArray *) types  limit: (int) limit onSucces: (void (^) (NSArray *response)) respondeHandler onError: (void (^) (NSError *error)) errorHandler
{
    NSString *urlAPI = [NSString stringWithFormat: @"%@?location=%f,%f&rankby=distance&sensor=false&types=%@&key=%@", API_GOOGLE_PLACE_URL, location.latitude, location.longitude, [types componentsJoinedByString: @"|"], API_GOOGLE_PLACE_KEY];
    
    NSLog(@"URL: %@", urlAPI);
    NSURL *url = [[NSURL alloc] initWithString: urlAPI];
    
    // Network Handler -> JSON
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSArray *venues = [XTPlacesFactory parseGoogleAPIPlacesResponse: (NSDictionary *) JSON];
                                             NSLog(@"Venues: %@", venues);
                                             respondeHandler(venues);
                                         }
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            errorHandler(error);
                                                                                        }];
    
    [operation start];
}

@end
