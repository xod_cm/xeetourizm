//
//  PlaceViewController.h
//  XeeTourizm
//
//  Created by Morgan Collino on 02/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MKMapView;
@class XTPlaces;

@interface PlaceViewController : UIViewController <UIAlertViewDelegate>
{
    UILabel *name;
    UILabel *streetName;
    UILabel *city;
    MKMapView *mapView;
    UILabel *distance;
    
    UIButton *itinerate;
    UIImageView  *image;
    
    XTPlaces *place;
}

@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) IBOutlet UILabel *name;
@property (nonatomic, retain) IBOutlet UILabel *streetName;
@property (nonatomic, retain) IBOutlet UILabel *city;
@property (nonatomic, retain) IBOutlet UILabel *distance;
@property (nonatomic, retain) IBOutlet UIButton *itinerate;
@property (nonatomic, retain) IBOutlet UIImageView *image;

@property (nonatomic, retain) XTPlaces *place;

@end
