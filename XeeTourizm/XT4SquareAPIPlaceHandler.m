//
//  XT4SquareAPIPlaceHandler.m
//  XeeTourizm
//
//  Created by Morgan Collino on 01/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "XT4SquareAPIPlaceHandler.h"
#import "AFJSONRequestOperation.h"
#import "XTPlacesFactory.h"

@implementation XT4SquareAPIPlaceHandler

+ (void) searchPlace: (CLLocationCoordinate2D) location forQuery: (NSString *) query  limit: (int) limit onSucces: (void (^) (NSArray *response)) respondeHandler onError: (void (^) (NSError *error)) errorHandler
{
    NSString *urlAPI = [NSString stringWithFormat: @"%@?v=%@&oauth_token=%@&query=%@&limit=%d&ll=%f,%f", API_4SQUARE_URL, API_4SQUARE_VERSION_API, API_4SQUARE__KEY, query, limit, location.latitude, location.longitude];
    
    NSURL *url = [[NSURL alloc] initWithString: urlAPI];
    
    // Network Handler -> JSON
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        NSArray *venues = [XTPlacesFactory parse4sqAPIResponse: (NSDictionary *) JSON];
        NSLog(@"Venues: %@", venues);
        respondeHandler(venues);
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        errorHandler(error);
    }];
    
    [operation start];
}

@end
