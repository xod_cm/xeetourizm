//
//  QuestionViewController.h
//  XeeTourizm
//
//  Created by Morgan Collino on 03/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionViewController : UIViewController
{
    UILabel *label;
    UIButton *yes;
    UIButton *no;
    NSString *tokenSearch;
}

@property (nonatomic, retain) IBOutlet UILabel *label;
@property (nonatomic, retain) IBOutlet UIButton *yes;
@property (nonatomic, retain) IBOutlet UIButton *no;
@property (nonatomic) NSString *tokenSearch;

@end
