//
//  CategoryCollectionViewCell.m
//  XeeTourizm
//
//  Created by Morgan Collino on 11/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "CategoryCollectionViewCell.h"

@implementation CategoryCollectionViewCell

@synthesize image = _image, label = _label;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
