//
//  XTTry.h
//  XeeTourizm
//
//  Created by Morgan Collino on 01/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XTTry : NSObject

+ (NSString *) tryGettingString: (NSDictionary *) dictionary forKey: (NSObject *)key defaultValue: (NSString *) defaultValue;
+ (NSString *) tryGettingString: (NSDictionary *) dictionary forKey: (NSObject *)key;

+ (double) tryGettingDouble: (NSDictionary *) dictionary forKey: (NSObject *)key defaultValue: (double) defaultValue;
+ (double) tryGettingDouble: (NSDictionary *) dictionary forKey: (NSObject *)key;

+ (NSNumber *) tryGettingNumber: (NSDictionary *) dictionary forKey: (NSObject *)key defaultValue: (NSNumber *) defaultValue;
+ (NSNumber *) tryGettingNumber: (NSDictionary *) dictionary forKey: (NSObject *)key;

@end
