//
//  XT4SquareAPIPlaceHandler.h
//  XeeTourizm
//
//  Created by Morgan Collino on 01/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define API_4SQUARE__KEY @"RSN2X1WJART2FX2KVZWAKCTBQUBF3UZSDHF3S5KFNB4NKNK1"
#define API_4SQUARE_VERSION_API  @"20130823"
#define API_4SQUARE_URL @"https://api.foursquare.com/v2/venues/search"

@interface XT4SquareAPIPlaceHandler : NSObject

+ (void) searchPlace: (CLLocationCoordinate2D) location forQuery: (NSString *) query  limit: (int) limit onSucces: (void (^) (NSArray *response)) respondeHandler onError: (void (^) (NSError *)) error;

@end
