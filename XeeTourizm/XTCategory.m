//
//  XTCategory.m
//  XeeTourizm
//
//  Created by Morgan Collino on 08/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "XTCategory.h"

@interface  XTCategory ()
{
}

@end

static NSArray *categories = nil;

static NSDictionary *subCategories = nil;

@implementation XTCategory



// [Sorties, Magasins, Transport, Utilitaires, Education, Religions, Beauté, Santé]


+ (NSArray *) categories
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        [array addObject: @"Sorties"];
        [array addObject: @"Magasins"];
        [array addObject: @"Transport"];
        [array addObject: @"Utilitaires"];
        [array addObject: @"Education"];
        [array addObject: @"Religion"];
        [array addObject: @"Beauté"];
        [array addObject: @"Santé"];
        
        categories = array;
    });
    
    return categories;
}

+ (void) initSubCategories
{
    NSMutableDictionary *mut = [[NSMutableDictionary alloc] init];
    [mut setObject: @{@"Sorties": @{@"Parc attractif": @"amusement_park", @"Galerie d'art": @"art_gallery", @"Bar": @"bar", @"Bowling": @"bowling_alley", @"Café": @"cafe", @"Casino": @"casino", @"Cinéma":@"movie_theater", @"Musée": @"museum", @"Boite de nuits" : @"night_club", @"Parc": @"park", @"Restaurant": @"restaurant", @"Zoo" : @"zoo"} , @"Magasins": @{ @"Vetements": @"clothing_store", @"Fleuriste": @"florist", @"Supermarché": @"grocery_or_supermarket", @"Chaussures": @"shoe_store", @"Centre commercial": @"shopping_mall"}, @"Transport" : @{@"Aéroport": @"airport", @"Stations bus": @"bus_station", @"Parking":@"parking", @"Station métro": @"subway_station", @"Taxis":@"taxi_stand", @"Gare":@"train_station", @"Lave auto" : @"car_wash", @"Station service" : @"gas_station"}, @"Utilitaires":@{@"Atm":@"atm", @"Banque": @"bank", @"Ambassade": @"embassy", @"Hopital" : @"hospital", @"Police" : @"police", @"Poste": @"post_office", @"Stade": @"stadium"}, @"Education": @{@"Librairie": @"library", @"Ecole": @"school", @"Université" : @"university"}, @"Religion":@{@"Eglise" : @"church", @"Cimetiere": @"cemetery", @"Temple hindou" : @"hindu_temple", @"Mosquée" : @"mosque", @"Synagogue" : @"synagogue"}, @"Beauté":@{@"Salon de beauté" :@"beauty_salon", @"Spa":@"spa", @"Coiffeur": @"hair_care"}, @"Santé":@{@"Docteur" :@"doctor", @"Pharmacie" :@"pharmacy", @"Hopital" : @"hospital"}} forKey: @"api_google"];
    [mut setObject: @{@"Sorties": @[], @"Magasins": @[], @"Transport" : @[], @"Utilitaires":@[], @"Education": @[], @"Religion":@[], @"Beauté":@[], @"Santé":@[]}  forKey: @"api_4sq"];
    [mut setObject: @{@"Sorties": @[], @"Magasins": @[], @"Transport" : @[], @"Utilitaires":    @[], @"Education": @[], @"Religion":@[], @"Beauté":@[], @"Santé":@[]}  forKey: @"api_tmw"];
    
    
    subCategories = mut;
}


+ (NSDictionary *) subCategoriesForCategory:(NSString *)category
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        
        if (subCategories == nil)
        {
            [self initSubCategories];
        }
    });
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject: [[subCategories objectForKey: @"api_google"] objectForKey:category] forKey:@"google_api"];
    [dic setObject: [[subCategories objectForKey: @"api_4sq"] objectForKey:category] forKey:@"4sq_api"];
    [dic setObject: [[subCategories objectForKey: @"api_tmw"] objectForKey:category] forKey:@"tmw_api"];
    
    return dic;
}
@end
