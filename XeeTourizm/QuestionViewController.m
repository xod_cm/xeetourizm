//
//  QuestionViewController.m
//  XeeTourizm
//
//  Created by Morgan Collino on 03/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "QuestionViewController.h"
#import "RootViewController.h"
#import "WarningView.h"
#import "PlaceTableViewController.h"
#import "XTTellMeWherePlaceHandler.h"
#import "XTUtils.h"
#import "XTPlaces.h"
#import "MBProgressHUD.h"

@interface QuestionViewController ()

@end

@implementation QuestionViewController

@synthesize label = _label, yes = _yes, no = _no, tokenSearch = _tokenSearch;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)noAction:(id)sender
{
    RootViewController *c = (RootViewController *)[self parentViewController];
    [[c container] setHidden: YES];
    [[c wView] setHidden: NO];
}

- (IBAction)yesAction:(id)sender
{
    RootViewController *c = (RootViewController *)[self parentViewController];
    [[c container] setHidden: YES];
    
    
    MBProgressHUD *hud  = [MBProgressHUD showHUDAddedTo:c.view animated:YES];
    hud.labelText = @"Loading";
    
    [XTTellMeWherePlaceHandler searchPlace: [[XeeManager sharedInstance] location] forCategorie: _tokenSearch limit:-42 onSucces:^(NSArray *response)
     {
         NSLog(@"Response: %@", response);
         
         NSMutableArray *tmp = [[NSMutableArray alloc] init];
         for (XTPlaces *t in response)
         {
             if (t.distance == 0.0)
             {
                 t.distance = [XTUtils distanceFromFirstCoordinate: [[XeeManager sharedInstance] location] toSecondCoordinate:t.location];
             }
             [tmp addObject: t];
         }
         
         [tmp sortUsingComparator:^(XTPlaces *firstObject, XTPlaces *secondObject)
          {
              CLLocationCoordinate2D firstKey = [firstObject location];
              CLLocationCoordinate2D secondKey = [secondObject location];
              
              double d1 = [XTUtils distanceFromFirstCoordinate: [[XeeManager sharedInstance] location] toSecondCoordinate: firstKey];
              double d2 = [XTUtils distanceFromFirstCoordinate: [[XeeManager sharedInstance] location] toSecondCoordinate: secondKey];
              
              if (d1 > d2)
                  return NSOrderedDescending;
              if (d2 > d1)
                  return NSOrderedAscending;
              return NSOrderedSame;
          }];
         
         dispatch_async(dispatch_get_main_queue(), ^{
             [MBProgressHUD hideHUDForView:c.view animated:YES];
         });

         
         PlaceTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"placeTableViewController"];
         controller.arrayPlaces = tmp;
         [self.navigationController pushViewController: controller animated: YES];
         
         
         
     } onError:^(NSError *error) {
         
         NSLog(@"Error: %@", error.description);
         dispatch_async(dispatch_get_main_queue(), ^{
             [MBProgressHUD hideHUDForView:c.view animated:YES];
         });
     }];
    
    

    
    //[self ]
}

@end
