//
//  VehicleStateController.m
//  XeeTourizm
//
//  Created by Morgan Collino on 08/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "VehicleStateController.h"
#import "XEE.h"

@interface VehicleStateController ()

@end

@implementation VehicleStateController

@synthesize rpm = _rpm, decelerate = _decelerate, accelerate = _accelerate, speed = _speed, voltage = _voltage, lock = _lock, acc = _acc, drive = _drive, frein = _frein;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[XeeManager sharedInstance] setDelegate: self];

    self.navigationController.navigationBar.hidden = NO;
    @try
    {
        XEEDataService *datas = [[XEEController sharedInstance] dataService];

        
        DQData *drpm = [datas dataForSignal: [[DQUtils sharedController] SignalNameEngineSpeed]];
        DQData *dspeed = [datas dataForSignal: [[DQUtils sharedController] SignalNameVehiculeSpeed]];
        
        //DQData *drive = [datas dataForSignal: [[DQUtils sharedController] SignalNameDriveMode]];
        
        [_rpm setText: [NSString stringWithFormat: @"%.2f",  [drpm value]]];
        [_speed setText: [NSString stringWithFormat: @"%.2f",  [dspeed value]]];
        [_decelerate setProgress:0.01];
        [_accelerate setProgress:0.01];
        
        [_rpm setText: [NSString stringWithFormat: @"%.2f",  [drpm value]]];

    }
    @catch (NSException *exception) {
        
    }
    
   	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma XeeManagerDelegate

- (void) didReceiveNewVehiculeSpeed: (double) value
{
    [_speed setText: [NSString stringWithFormat: @"%.2f", value]];
}


- (void) didReceiveNewBrakePosition: (double) value
{
    double ratio = (value / 20.0);
    if (ratio == 0.0)
        ratio = 0.01;
    [_decelerate setProgress: ratio];
}

- (void) didReceiveNewThrottlePosition: (double) value
{
    double ratio = (value / 20.0);
    if (ratio == 0.0)
        ratio = 0.01;
    [_accelerate setProgress: ratio];
}

- (void) didReceiveNewRPM: (double) value
{
    [_rpm setText: [NSString stringWithFormat: @"%.2f", value]];
}

- (void) didReceiveNewVoltage: (double) value
{
    [_voltage setText: [NSString stringWithFormat: @"%.2f", value]];
}

- (void) didReceiveNewDriveMode:(double)value
{
    if (value == 0.0)
    {
        [_drive setAlpha: 0.2];
    }
    else
        [_drive setAlpha: 1];
}

- (void) didReceiveNewLockSts:(double)value
{
    if (value != 0.0)
        [_lock setImage: [UIImage imageNamed: @"lock.png"]];
    else
        [_lock setImage: [UIImage imageNamed: @"unlock.png"]];
}

- (void) didReceiveNewBrakePedalSts: (double) value
{
    if (value == 0.0)
    {
        [_frein setImage: [UIImage imageNamed: @"frein_g.png"]];
        [_frein setAlpha: 0.45];
    }
    else
    {
        [_frein setImage: [UIImage imageNamed: @"frein_b.png"]];
        [_frein setAlpha: 0.85];
    }
}

- (void) didReceiveNewThrottleSts: (double) value
{
    if (value == 0.0)
    {
        [_acc setImage: [UIImage imageNamed: @"acc_g.png"]];
        [_acc setAlpha: 0.45];
    }
    else
    {
        [_acc setImage: [UIImage imageNamed: @"acc_r.png"]];
        [_acc setAlpha: 0.85];
    }
}



@end
