//
//  VehicleStateController.h
//  XeeTourizm
//
//  Created by Morgan Collino on 08/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XeeManager.h"

@interface VehicleStateController : UIViewController <XeeManagerDelegate>
{
    UIProgressView *accelerate;
    UIProgressView *decelerate;
    
    UILabel *rpm;
    UILabel *speed;
    UIImageView *lock;
    UIImageView *drive;
    UIImageView *frein;
    UIImageView *acc;
}

@property (nonatomic, retain) IBOutlet UIProgressView *accelerate;
@property (nonatomic, retain) IBOutlet UIProgressView *decelerate;
@property (nonatomic, retain) IBOutlet UILabel *rpm;
@property (nonatomic, retain) IBOutlet UILabel *speed;
@property (nonatomic, retain) IBOutlet UILabel *voltage;

@property (nonatomic, retain) IBOutlet UIImageView *lock;
@property (nonatomic, retain) IBOutlet UIImageView *drive;
@property (nonatomic, retain) IBOutlet UIImageView *frein;
@property (nonatomic, retain) IBOutlet UIImageView *acc;


@end
