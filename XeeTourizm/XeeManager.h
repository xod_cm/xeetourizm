//
//  XeeManager.h
//  XeeTourizm
//
//  Created by Morgan Collino on 26/08/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "XEE.h"

@protocol XeeManagerDelegate <NSObject>

@optional
- (void) didAuthenticate;
- (void) didReceiveNewVehiculeSpeed: (double) value;
- (void) didReceiveNewLocation: (CLLocationCoordinate2D) location;
- (void) didReceiveNewFuelLevel: (double) value;
- (void) didReceiveNewBrakePosition: (double) value;
- (void) didReceiveNewThrottlePosition: (double) value;
- (void) didReceiveNewRPM: (double) value;
- (void) didReceiveNewVoltage: (double) value;
- (void) didReceiveNewDriveMode: (double) value;
- (void) didReceiveNewLockSts: (double) value;
- (void) didReceiveNewBrakePedalSts: (double) value;
- (void) didReceiveNewThrottleSts: (double) value;

- (void) alertRunOutGas;

@end


#define LIMIT_GAS 12

@interface XeeManager : NSObject <XEEControllerDelegate, CLLocationManagerDelegate>
{
    id<XeeManagerDelegate> delegate;
    CLLocationCoordinate2D location;
    BOOL isConnected;
}

+ (XeeManager *) sharedInstance;


@property (nonatomic) CLLocationCoordinate2D location;
@property (nonatomic, weak) id<XeeManagerDelegate> delegate;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic) BOOL isConnect;

@end
