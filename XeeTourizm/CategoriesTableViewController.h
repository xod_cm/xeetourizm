//
//  CategoriesTableViewController.h
//  XeeTourizm
//
//  Created by Morgan Collino on 10/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesTableViewController : UITableViewController
{
    NSArray *categories;
    NSDictionary *categoriesWithKey;
    // @"category1": @{@"google_api":@"category_1", @"tmw_api": @"cat_1", @"4sq_api":@"cate_1"}
}

@property (nonatomic) NSArray *categories;
@property (nonatomic) NSDictionary *categoriesWithKey;

@end
