//
//  LoadingViewController.h
//  XeeTourizm
//
//  Created by Morgan Collino on 06/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XeeManager.h"

@interface LoadingViewController: UIViewController <XeeManagerDelegate>
{
    UIActivityIndicatorView *loader;
    UILabel *label;
    UILabel *appName;
}

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *loader;
@property (nonatomic, retain) IBOutlet UILabel *label;
@property (nonatomic, retain) IBOutlet UILabel *appName;


@end
