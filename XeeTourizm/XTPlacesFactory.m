//
//  XTPlacesFactory.m
//  XeeTourizm
//
//  Created by Morgan Collino on 01/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "XTPlacesFactory.h"
#import "XTPlaces.h"
#import "XTTry.h"

@implementation XTPlacesFactory


// Parse Google API Places
+ (XTPlaces *) parseGoogleAPIPlacesNode: (NSDictionary *) node
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    // Bind data from Node to Dictionary
    NSDictionary *geometry = [node objectForKey: @"geometry"];
    NSDictionary *location = [geometry objectForKey: @"location"];
    
    NSString *vicinity = [XTTry tryGettingString: node forKey: @"vicinity"];
    NSArray *split = [vicinity componentsSeparatedByString:@", "];
    
    if ([split count] == 2)
    {
        [dictionary setObject: [split objectAtIndex: 0] forKey: @"streetName"];
        [dictionary setObject: [split objectAtIndex: 1] forKey: @"city"];
    }
    else
        [dictionary setObject: vicinity forKey: @"streetName"];

    
    [dictionary setObject: [XTTry tryGettingNumber: location forKey: @"lng"] forKey: @"longitude"];
    [dictionary setObject: [XTTry tryGettingNumber: location forKey: @"lat"] forKey: @"latitude"];
    [dictionary setObject: [XTTry tryGettingString: node forKey: @"name"] forKey: @"name"];
    
    XTPlaces *value = [[XTPlaces alloc] initWithDictionary: dictionary];
    return value;
}

+ (NSArray *) parseGoogleAPIPlacesResponse: (NSDictionary *) data
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSDictionary *results = [data objectForKey: @"results"];
    
    @try
    {
        for (NSDictionary *node in results)
        {
            XTPlaces *place = [self parseGoogleAPIPlacesNode: node];
            [array addObject: place];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception: %@", exception.description);
    }
    return array;
}

// Parser Foursquare API (search places)
+ (XTPlaces *) parse4sqAPINode: (NSDictionary *) node
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    // Bind data from Node to Dictionary
    NSDictionary *location = [node objectForKey: @"location"];
    
    [dictionary setObject: [XTTry tryGettingNumber: location forKey: @"lng"] forKey: @"longitude"];
    [dictionary setObject: [XTTry tryGettingNumber: location forKey: @"lat"] forKey: @"latitude"];
    [dictionary setObject: [XTTry tryGettingString: node forKey: @"name"] forKey: @"name"];
    [dictionary setObject: @"" forKey: @"streetName"];
    [dictionary setObject: [XTTry tryGettingString: location forKey: @"city"] forKey: @"city"];
    [dictionary setObject: @"" forKey: @"description"];
    [dictionary setObject: [XTTry tryGettingString: location forKey: @"state"] forKey: @"state"];
    [dictionary setObject: [XTTry tryGettingString: location forKey: @"country"] forKey: @"country"];
    [dictionary setObject: [XTTry tryGettingNumber: location forKey:@"distance"] forKey: @"distance"];
    
    
    XTPlaces *value = [[XTPlaces alloc] initWithDictionary: dictionary];
    return value;
}

+ (NSArray *) parse4sqAPIResponse: (NSDictionary *) data
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    @try
    {
        NSDictionary *response = [data objectForKey: @"response"];
        NSArray *venues = [response objectForKey: @"venues"];
        
        for (NSDictionary *node in venues)
        {
            XTPlaces *place = [self parse4sqAPINode: node];
            [array addObject: place];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception: %@", exception.description);
    }

    return array;
}



// Parser TellMeWhere
+ (XTPlaces *) parseTMWAPINode:(NSDictionary *)node
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    // Bind data from Node to Dictionary
    NSDictionary *location = [node objectForKey: @"position"];
    NSDictionary *address = [node objectForKey: @"address"];
    
    [dictionary setObject: [XTTry tryGettingNumber: location forKey: @"lng"] forKey: @"longitude"];
    [dictionary setObject: [XTTry tryGettingNumber: location forKey: @"lat"] forKey: @"latitude"];
    
    
    [dictionary setObject: [XTTry tryGettingString: node forKey: @"name"] forKey: @"name"];
    [dictionary setObject: [XTTry tryGettingString: address forKey:@"street"] forKey: @"streetName"];
    [dictionary setObject: [XTTry tryGettingString: address forKey: @"city"] forKey: @"city"];
    [dictionary setObject: @"" forKey: @"description"];
    [dictionary setObject: [XTTry tryGettingString: location forKey: @"state"] forKey: @"state"];
    [dictionary setObject: [XTTry tryGettingString: address forKey: @"country"] forKey: @"country"];
       
    XTPlaces *value = [[XTPlaces alloc] initWithDictionary: dictionary];
    return value;
}

+ (NSArray *) parseTMWAPIResponse:(NSDictionary *)data
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    @try
    {
        NSDictionary *response = [data objectForKey: @"places"];
        NSArray *venues = [response objectForKey: @"place"];
        
        for (NSDictionary *node in venues)
        {
            XTPlaces *place = [self parseTMWAPINode: node];
            [array addObject: place];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception: %@", exception.description);
    }
    
    return array;
}


@end
