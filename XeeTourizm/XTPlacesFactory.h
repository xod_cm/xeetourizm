//
//  XTPlacesFactory.h
//  XeeTourizm
//
//  Created by Morgan Collino on 01/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XTPlaces;

@interface XTPlacesFactory : NSObject


// Parse Google API Places
+ (XTPlaces *) parseGoogleAPIPlacesNode: (NSDictionary *) values;
+ (NSArray *) parseGoogleAPIPlacesResponse: (NSDictionary *) data;

// Parser Foursquare API (search places)
+ (XTPlaces *) parse4sqAPINode: (NSDictionary *) node;
+ (NSArray *) parse4sqAPIResponse: (NSDictionary *) data;


// Parser TellMeWhere API (search places)
+ (XTPlaces *) parseTMWAPINode: (NSDictionary *) node;
+ (NSArray *) parseTMWAPIResponse: (NSDictionary *) data;


@end
