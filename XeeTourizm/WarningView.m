//
//  WarningView.m
//  XeeTourizm
//
//  Created by Morgan Collino on 03/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "WarningView.h"
#import "QuestionViewController.h"
#import "RootViewController.h"

@implementation WarningView
@synthesize label = _label, image = _image, button = _button, close = _close, delegate = _delegate, textQuestion = _textQuestion, tokenSearch = _tokenSearch;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setText: (NSString *) text
{
    [_label setText: text];
}

- (IBAction)touchAction:(id)sender
{
    // TO DO
    [[_delegate container] setHidden: NO];
    QuestionViewController *question = [[_delegate childViewControllers] objectAtIndex: 0];
    [[question label] setText: _textQuestion];
    [question setTokenSearch: _tokenSearch];
    [super setHidden: YES];

}

- (IBAction)closeAction:(id)sender
{
    // TO DO
    super.hidden = YES;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
