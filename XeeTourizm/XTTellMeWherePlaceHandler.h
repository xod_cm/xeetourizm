//
//  XTTellMeWherePlaceHandler.h
//  XeeTourizm
//
//  Created by Morgan Collino on 06/09/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


#define API_TMW__KEY @"zhmGt9srO80c"
#define API_TMW_URL @"http://api.tellmewhere.com/v1/searchplaces2.json"

@interface XTTellMeWherePlaceHandler : NSObject

+ (void) searchPlace: (CLLocationCoordinate2D) location forCategorie: (NSString *) query  limit: (int) limit onSucces: (void (^) (NSArray *response)) respondeHandler onError: (void (^) (NSError *)) error;
@end
