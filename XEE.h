//
//  XEE.h
//  XeeSDK
//
//  Created by Mathieu Dubois on 08/07/13.
//  Copyright (c) 2013 Eliocity. All rights reserved.
//
#import <Foundation/Foundation.h>

extern NSString *const XEEAuthErrorDomain;
extern NSString *const XEEApiErrorDomain;
extern NSString *const XEERealtimeErrorDomain;

@class XEEController, DtcSignal;


/*!
 \brief Global variables definition
 \note  Definitions
 */
@interface DQUtils : NSObject {
}

typedef NSNumber SignalName;


/*!
 \brief Signal Name used to identify a vehicle signal
 \note  Used in the NSArray passed by the user to the setDesiredSignals:(NSArray*) signalNameArray method of UserVehicle. Used as Signal Name into Data objects.
 */
@property(nonatomic, readonly) SignalName *SignalNameNoSignal;                          //!< No signal (default)
@property(nonatomic, readonly) SignalName *SignalNameOdometer;                          //!< Odometer
@property(nonatomic, readonly) SignalName *SignalNameVehiculeSpeed;                     //!< Vehicle speed
@property(nonatomic, readonly) SignalName *SignalNameFuelLevel;                         //!< Fuel level
@property(nonatomic, readonly) SignalName *SignalNameEngineSpeed;                       //!< Engine speed (RPM)
@property(nonatomic, readonly) SignalName *SignalNameSteeringWheelAngle;                //!< Steering wheel angle
@property(nonatomic, readonly) SignalName *SignalNameSteeringWheelSide;                 //!< Steering wheel side
@property(nonatomic, readonly) SignalName *SignalNameFrontLeftWheelSpeed;               //!< Front left wheel speed
@property(nonatomic, readonly) SignalName *SignalNameFrontRightWheelSpeed;              //!< Front right wheel speed
@property(nonatomic, readonly) SignalName *SignalNameRearLeftWheelSpeed;                //!< Rear left wheel speed
@property(nonatomic, readonly) SignalName *SignalNameRearRightWheelSpeed;               //!< Rear right wheel speed
@property(nonatomic, readonly) SignalName *SignalNameLockSts;                           //!< Lock status
@property(nonatomic, readonly) SignalName *SignalNameBrakePedalPosition;                //!< Brake pedal position
@property(nonatomic, readonly) SignalName *SignalNameBrakePedalSts;                     //!< Brake pedal status
@property(nonatomic, readonly) SignalName *SignalNameBatteryVoltage;                    //!< Battery voltage
@property(nonatomic, readonly) SignalName *SignalNameFrontLeftWindowPosition;           //!< Front left window position
@property(nonatomic, readonly) SignalName *SignalNameFrontLeftWindowSts;                //!< Front left window status
@property(nonatomic, readonly) SignalName *SignalNameFrontRightWindowPosition;          //!< Front right window position
@property(nonatomic, readonly) SignalName *SignalNameFrontRightWindowSts;               //!< Front right window status
@property(nonatomic, readonly) SignalName *SignalNameRearLeftWindowPosition;            //!< Rear left window position
@property(nonatomic, readonly) SignalName *SignalNameRearLeftWindowSts;                 //!< Rear left window status
@property(nonatomic, readonly) SignalName *SignalNameRearRightWindowPosition;           //!< Rear right window position
@property(nonatomic, readonly) SignalName *SignalNameRearRightWindowSts;                //!< Rear right window status
@property(nonatomic, readonly) SignalName *SignalNameThrottlePedalPosition;             //!< Throttle pedal position
@property(nonatomic, readonly) SignalName *SignalNameThrottlePedalSts;                  //!< Throttle pedal status
@property(nonatomic, readonly) SignalName *SignalNameHazardSts;                         //!< Hazard status
@property(nonatomic, readonly) SignalName *SignalNameLowBeamSts;                        //!< Low beam status
@property(nonatomic, readonly) SignalName *SignalNameRearFogLightSts;                   //!< Rear fog light status
@property(nonatomic, readonly) SignalName *SignalNameHighBeamSts;                       //!< High beam status
@property(nonatomic, readonly) SignalName *SignalNameHeadLightSts;                      //!< Head lights status
@property(nonatomic, readonly) SignalName *SignalNameTrunkSts;                          //!< Trunk staus
@property(nonatomic, readonly) SignalName *SignalNameLeftIndicatorSts;                  //!< Left indicator status
@property(nonatomic, readonly) SignalName *SignalNameRightIndicatorSts;                 //!< Right indicator status
@property(nonatomic, readonly) SignalName *SignalNameFrontFogLightSts;                  //!< Front fog light status
@property(nonatomic, readonly) SignalName *SignalNameFrontLeftDoorSts;                  //!< Front left door status
@property(nonatomic, readonly) SignalName *SignalNameFrontRightDoorSts;                 //!< Front right door status
@property(nonatomic, readonly) SignalName *SignalNameRearLeftDoorSts;                   //!< Rear left door status
@property(nonatomic, readonly) SignalName *SignalNameRearRightDoorSts;                  //!< Rear right door status
@property(nonatomic, readonly) SignalName *SignalNameHandBrakeSts;                      //!< Hand brake status
@property(nonatomic, readonly) SignalName *SignalNameFrontLeftSeatBeltSts;              //!< Front left seat belt status
@property(nonatomic, readonly) SignalName *SignalNameFrontRightSeatBeltSts;             //!< Front right seat belt status
@property(nonatomic, readonly) SignalName *SignalNamePassAirbagSts;                     //!< Passenger airbag status
@property(nonatomic, readonly) SignalName *SignalNameAutoWiperSts;                      //!< Auto wipers status
@property(nonatomic, readonly) SignalName *SignalNameIntermittentWiperSts;              //!< Intermittent wipers status
@property(nonatomic, readonly) SignalName *SignalNameLowSpeedWiperSts;                  //!< Low speed wipers status
@property(nonatomic, readonly) SignalName *SignalNameHighSpeedWiperSts;                 //!< High speed wipers status
@property(nonatomic, readonly) SignalName *SignalNameClutchPedalPosition;               //!< Clutch pedal position
@property(nonatomic, readonly) SignalName *SignalNameClutchPedalSts;                    //!< Clutch pedal status
@property(nonatomic, readonly) SignalName *SignalNameCruiseControlSts;                  //!< Cruise control status
@property(nonatomic, readonly) SignalName *SignalNameReverseGearSts;                    //!< Reverse gear status
@property(nonatomic, readonly) SignalName *SignalNameSunRoofSts;                        //!< Sun roof status
@property(nonatomic, readonly) SignalName *SignalNameRearWiperSts;                      //!< Rear wiper status
@property(nonatomic, readonly) SignalName *SignalNameManualWiperSts;                    //!< Manual wiper status
@property(nonatomic, readonly) SignalName *SignalNameRadioSts;                          //!< Radio status
@property(nonatomic, readonly) SignalName *SignalNameGearPosition;                      //!< Gear position
@property(nonatomic, readonly) SignalName *SignalNameHoodSts;                           //!< Hood status
@property(nonatomic, readonly) SignalName *SignalNameAirCondSts;                        //!< Air conditioning status
@property(nonatomic, readonly) SignalName *SignalNameVentilationSts;                    //!< Ventilation status
@property(nonatomic, readonly) SignalName *SignalNameIgnitionSts;                       //!< Ignition status
@property(nonatomic, readonly) SignalName *SignalNameWindowsLockSts;                    //!< Windows lock status
@property(nonatomic, readonly) SignalName *SignalNameKeySts;                            //!< Key status
@property(nonatomic, readonly) SignalName *SignalNameFuelCapSts;                        //!< Fuel cap status
@property(nonatomic, readonly) SignalName *SignalNameInteriorLightSts;                  //!< Interior lights status
@property(nonatomic, readonly) SignalName *SignalNameDriveMode;                         //!< Drive mode


@property(nonatomic, readonly) NSDictionary *namesForIds;


/*!
 \brief Init values
 \note  Default values for SignalName, UnitOfMeasure, Data.Value, Data.timestamp
 */
@property(nonatomic, readonly) SignalName *defaultDataName;
@property(nonatomic, readonly) long defaultDataValue;
@property(nonatomic, readonly) unsigned long defaultDataTimeStamp;
@property(nonatomic, readonly) double defaultMaximum;
@property(nonatomic, readonly) double defaultMinimum;
@property(nonatomic, readonly) BOOL defaultIsBlinking;
@property(nonatomic, readonly) BOOL defaultIsAbsolute;
@property(nonatomic, readonly) BOOL defaultIsInverted;
@property(nonatomic, readonly) BOOL defaultIsSigned;
@property(nonatomic, readonly) NSMutableDictionary *unitsOfMeasure;
@property(nonatomic, readonly) NSMutableDictionary *minValues;
@property(nonatomic, readonly) NSMutableDictionary *maxValues;



/*!
 \return The unique instance of this class
 \note  The macro 'definitions' defined in this header is equal to "[DQUtils SharedController]"
 */
+ (DQUtils *)sharedController;

/*!
 \param[in] signalName the desired SignalName
 \return the um corresponding to the selected SignalName
 */
- (NSString*) getUmForSignalName:(SignalName*)signalName;


/*!
 \param[in] signalName the desired SignalName
 \return the minimum value corresponding to the selected SignalName
 */
- (double) minValueForSignalName:(SignalName*)signalName;


/*!
 \param[in] signalName the desired SignalName
 \return the maximum value corresponding to the selected SignalName
 */
- (double) maxValueForSignalName:(SignalName*)signalName;

/*!
 \param[in] signalName the desired SignalName
 \return the name corresponding to the selected SignalName
 */
- (NSString*) getStringForSignalName:(NSNumber*)signalName;

@end


/*!
 \brief     DQData  class
 \details   DQData Data
 */
@interface DQData : NSObject{
    SignalName *name;               //!< The signalName enum describing the name of the signal (see DQuidDefinitions.h) expressed in the UnitOfMeasure. Readonly
    double value;                   //!< The current value of the signal. Readonly
    NSString *um;                   //!< The UnitOfMeasure of the signal (see DQuidDefinitions.h). Readonly
    unsigned long timestamp;        //!< Standard UNIX timestamp. The current timestamp is the number of seconds that have elapsed since January 1, 1970. Readonly
    BOOL received;                  //!< Boolean set to true if the value has just been received from the DQuid Unit.
    double maximum;                 //!< Max value of the signal. Readonly
    double minimum;                 //!< Min value of the signal. Readonly
    BOOL isSigned;                  //!< Inform if the signal’s value is signed (true). Readonly
    BOOL isInverted;                //!< Inform if the signal’s value is inverted (true). Readonly
    BOOL isBlinking;                //!< Inform if the signal’s value is blinking (true). Readonly
    BOOL isAbsolute;                //!< Inform if the signal’s value is absolute (true). Readonly
}


@property(nonatomic, readonly)  SignalName *name;
@property(nonatomic, readonly)  double value;
@property(nonatomic, readonly)  NSString *um;
@property(nonatomic, assign)    unsigned long timestamp;
@property(nonatomic, assign)    BOOL received;
@property(nonatomic, readonly)  BOOL isSigned;
@property(nonatomic, readonly)  BOOL isInverted;
@property(nonatomic, readonly)  BOOL isBlinking;
@property(nonatomic, readonly)  BOOL isAbsolute;
@property(nonatomic, readonly)  double minimum;
@property(nonatomic, readonly)  double maximum;

/*!
 \brief Initialization of the DQData class
 \note  Initialization of the DQData class
 \retVal id The DQData class.
 */
- (id) init;

/*!
 \brief Initialization of the DQData class specifying the SignalName, Value,  UnitOfMeasure, isSigned, isAbsolute, isInverted, isBlinking, Maximum e Minimum values.
 \note  Initialization of the DQData class specifying the SignalName, Value, UnitOfMeasure, isSigned, isAbsolute, isInverted, isBlinking, Maximum e Minimum values.
 \retVal id The data class.
 */
- (id) initWithName:(SignalName*)_name Value:(double)_value Um:(NSString*)_um Inverted:(BOOL)_inverted Signed:(BOOL)_signed Blinking:(BOOL)_blinking Absolute:(BOOL)_absolute Maximum:(BOOL)_maximum Minimum:(BOOL)_minimum;

/*!
 \brief Initialization of the DQData class specifying the SignalName, Value, UnitOfMeasure, timestamp, isSigned, isAbsolute, isInverted, isBlinking, Maximum e Minimum values.
 \note  Initialization of the DQData class specifying the SignalName, Value, UnitOfMeasure, timestamp, isSigned, isAbsolute, isInverted, isBlinking, Maximum e Minimum values.
 \retVal id The data class.
 */
- (id) initWithName:(SignalName*)_name Value:(double)_value Um:(NSString*)_um Inverted:(BOOL)_inverted Signed:(BOOL)_signed Blinking:(BOOL)_blinking Absolute:(BOOL)_absolute Maximum:(BOOL)_maximum Minimum:(BOOL)_minimum Timestamp:(unsigned long)_timestamp;

@end

@interface XEEService : NSObject

@end


/**
 * \warning Abstract class - DO NOT INSTANCIATE
 */
@interface XEEEntity : NSObject

/**
 * \brief initialization method using an NSDictionary
 */
- (id)initWithDictionary:(NSDictionary *)dict;
/**
 * \brief representation of the entity as an NSDictionary
 * \return NSDictionary
 */
- (NSDictionary *)dictionaryWithValuesForAllKeys;


@end

/**
 * \brief Representation of a registered Xee user
 */
@interface XEEUser : XEEEntity


@property (strong, nonatomic) NSNumber *id; //!< \brief user's unique id
@property (strong, nonatomic) NSString * name; //!< \brief user's name
@property (strong, nonatomic) NSString * firstName; //!< \brief user's firstname
@property (strong, nonatomic) NSNumber * gender; //!< \brief user's gender. A number with an integer value of 0 corresponds to a male. An integer value of 1 corresponds to a female
@property (strong,nonatomic) NSString * nickName; //!< \brief user's nickname
@property (strong,nonatomic) NSString * role; //!< \brief the role of the user. Possible values are @"user", @"dev", @"admin"
@property (strong,nonatomic) NSDate * birthDate; //!<\brief the user's date of birth
@property (strong,nonatomic) NSDate * licenseDeliveryDate; //!< \brief the date the user's driver license has been issued

@end

/**
 * \brief represents an email address
 */
@interface XEEEmailAddress : XEEEntity

@property (nonatomic, strong) NSNumber *id; //!< \brief unique identifier
@property (nonatomic, strong) NSString *emailAddress; //!< \brief the email address itself
@property (nonatomic, assign) BOOL isMainEmailAddress; //!< \brief boolean indicating if this is the main email address of the user
@property (nonatomic, assign) BOOL optinEliocity;
@property (nonatomic, assign) BOOL optinPartners;

@end

/**
 * \brief represents a phone number
 */
@interface XEEPhoneNumber : XEEEntity

@property (nonatomic, strong) NSNumber *id; //!< \brief unique identifier
@property (nonatomic, strong) NSString *phoneNumber; //!< \brief the phone number itself
@property (nonatomic, assign) BOOL optinEliocity;
@property (nonatomic, assign) BOOL optinPartners;

@end

/**
 * \brief represents a car
 */
@interface XEECar : XEEEntity

@property (strong, nonatomic) NSNumber *id; //!< \brief car's unique identifier
@property (strong, nonatomic) NSString *name; //!< \brief the name given by the user to his car
@property (strong, nonatomic) NSString *brand; //!< \brief car's brand
@property (strong, nonatomic) NSString *model; //!< \brief car's model
@property (strong, nonatomic) NSString *plateNumber; //!< \brief car's plate number
@property (strong, nonatomic) NSNumber *year; //!< car's year

@end

/**
 * \brief represents the order in wich the results of a XEEDataRequest will be fetched and returned
 */
typedef enum {
    XEEDataRequestOrderAsc,
    XEEDataRequestOrderDesc
} XEEDataRequestOrder;

/**
 * \brief represents the parameters of a request that will be made to xee's apis to fetch data
 */
@interface XEEDataRequest : NSObject

@property (assign, nonatomic) XEEDataRequestOrder order; //!< \see XEEDataRequestOrder
@property (strong, nonatomic) NSString *orderBy; //!< \brief the parameter by which the results will be sorted
@property (strong, nonatomic) NSDate *startDate; //!< \brief the begining of the interval in which the data will be fetched
@property (strong, nonatomic) NSDate *endDate; //!< \brief the end of the interval in which the data will be fetched
@property (assign, nonatomic) NSInteger limit; //!< \brief the number of results to fetch
@property (assign, nonatomic) NSInteger offset; //!< \brief if a request has more results than the `limit` your can use this parameter to fetch more results
@property (strong, nonatomic) SignalName *signalName; //!< \brief a specific signal to fetch

@end

/**
 * \brief Represents a registered OAuth client
 */
@interface XEEClient : NSObject

@property (nonatomic, strong) NSString *identifier; //!< \brief unique identifier
@property (nonatomic, strong) NSString *password; //!< \brief password
@property (nonatomic, strong) NSString *name; //!< \brief the name of the client that will be displayed on the connection view
@property (nonatomic, strong) NSURL *redirectUri; //!< \brief the URI to which the authentication server will redirect the user after the authentication
@property (nonatomic, strong) NSArray *scopes; //!< \brief an array of NSStrings corresponding to the scopes requested during authentication

/**
 * \brief the Client's initialisation method
 * \param[in] identifier the client's unique identifier
 * \param[in] password the client's password
 * \param[in] redirectUri the redirection URI
 * \param[in] scopes an array of scopes
 * \param[in] name the name of the client
 * \return an instance of XEEClient
 */
- (id)initWithIdentifier:(NSString *)identifier password:(NSString *)password redirectUri:(NSURL *)redirectUri scopes:(NSArray *)scopes name:(NSString *)name;

@end

/**
 * \brief an abstraction layer of xee's User apis
 */
@interface XEEUserService : XEEService

- (XEEUser *)currentUser:(NSError **)error; //!< \brief returns the authenticated user
- (XEEUser *)userWithId:(NSInteger)userId error:(NSError **)error; //!< \brief returns the user with the specified id

@end

/**
 * \brief this service allows you to manage a user's email addresses
 */
@interface XEEEmailAddressService : XEEService

- (NSArray *)emailAddressesForUSer:(XEEUser *)user error:(NSError **)error; //!< \brief returns a user's email addresses \return an array of XEEEmailAddress

@end

/**
 * \brief this service allows you to manage a user's phone numbers
 */
@interface XEEPhoneNumberService : XEEService

- (NSArray *)phoneNumbersForUSer:(XEEUser *)user error:(NSError **)error; //!< \brief returns a user's phone numbers \return an array of XEEPhoneNumber

@end

/**
 * \brief this service allows you to access a car data
 */
@interface XEEDataService : XEEService

- (DQData *)dataForSignal:(SignalName *)signal; //!< \brief return the current value for a signal

- (NSDictionary *)lastUpdatedData; //!< \return returns an NSDictionary with the latest updated signals with SignalName as keys and the corresponding Data as values
- (NSDictionary *)lastAvailableData;//!< \return returns an NSDictionary with the latest available signals with SignalName as keys and the corresponding Data as values

- (NSDictionary *)availableSignals;//!< \return returns an NSDictionary with the available signals with SignalName as keys and corresponding NSString as values

- (NSString *)createSharedReportForCarId:(NSInteger)carId error:(NSError **)error; //!< \brief creates a report to share the current position and status of a car. \param carId the identifier of the car \return the shortcode used to share the report
- (BOOL)updateBatteryLevelForSharedReportWithShortCode:(NSString *)shortCode forCarId:(NSInteger)carId error:(NSError **)error; //!< \brief updates the phone battery level for a given report
- (NSArray *)executeDataRequest:(XEEDataRequest *)request error:(NSError **)error; //!< fetches the data with the search parameters of the provided XEEDataRequest. \return an array of Data

@end

@interface XEECarService : XEEService

- (XEECar *)currentCar:(NSError **)error; //!< \brief returns the car currently associated with the device

@end

/**
 * \brief You must implement this protocol to allow the XEEController to communicate with your application.
 */
@protocol XEEControllerDelegate <NSObject>

/**
 * \name OAuth
 * @{
 */
- (void)XEEControllerDidAuthenticateUser; //!< Called when the User as been authenticated
- (void)XEEControllerDidFailAuthenticatingUserWithError:(NSError *)error; //!< Called when the User authentication failed
/** @} */


@optional


/**
 * \name Bluetooth pairing with the device
 * @{
 */
- (void)XEEControllerDidConectToXEEDevice; //!< Called when the connection with the device is successful
- (void)XEEControllerDidDisconnectFromXEEDevice; //!< Called when the connection has been terminated
- (void)XEEControllerDidFailConnectingToXEEDeviceWithError:(NSError *)error; //!< Called when the pairing failed
/** @} */

/**
 * \name Firmware Update
 * @{
 */
- (void)XEEDeviceNeedsFirmwareUpdate; //!< Called when a firmware update is needed
- (void)XEEDeviceDidStartUpdatingFirmware; //!< Called when the firmware update starts
- (void)XEEDeviceDidFinishUpdatingFirmware; //!< Called when the firmware update has finished
- (void)XEEDeviceFirmwareUpdateProgressDidChange:(double)progress; //!< Called when the progress of the firmware updated changed
/** @} */

/**
 * \name Data fetching
 * @{
 */
- (void)XEEDeviceDidReceiveNewDataForSignals:(NSArray *)signals; //!< Called when new data is available - signals is an array of SignalName
/** @} */


#pragma mark - DQuid delegate protocol deprecated

/**
 * \name Deprecated
 * @{
 */

- (void) onConnectionSuccessful:(int)vehicleId; //!< \deprecated \see XEEControllerDidConectToXEEDevice
- (void) onNewData:(NSArray*)newData; //!< \deprecated \see XEEDeviceDidReceiveNewDataForSignals:(NSArray *)signals
- (void) onErrorOccurred:(NSError*)error; //!< \deprecated \see XEEControllerDidFailConnectingToXEEDeviceWithError:(NSError *)error
- (void) onConnectionClosed; //!< \deprecated \see XEEControllerDidDisconnectFromXEEDevice
- (void) onConnectionOpened; //!< \deprecated \see XEEControllerDidConectToXEEDevice
- (void) onDtcNumber:(DtcSignal*)dtcSignal; //!< \deprecated
- (void) onDtcCodes:(DtcSignal*)dtcSignal; //!< \deprecated
- (void) onFirmwareUpdateStarted; //!< \deprecated \see XEEDeviceDidStartUpdatingFirmware
- (void) onFirmwareUpdateCompleted; //!< \deprecated \see XEEDeviceDidFinishUpdatingFirmware
- (void) onFirmwareUpdateNeeded; //!< \deprecated \see XEEDeviceNeedsFirmwareUpdate
- (void) onFirmwareUpdateIncrease:(double)percentage; //!< \deprecated \see XEEDeviceFirmwareUpdateProgressDidChange:(double)progress

/** @} */

@end

/**
 \brief the XEEController is a singleton that allows you to interact with the Xee Device. It manages the user authentication using OAuth and the connection to the device via bluetooth
 
 ##Usage example:##
 ~~~~~~~~~~{.m}
 XEEController *xee = [XEEController sharedInstance];
 ~~~~~~~~~~
 */
@interface XEEController : NSObject

@property (strong, nonatomic) XEEClient *client; //!< \brief the OAuth client
@property (strong, nonatomic, readonly) XEEUserService *userService; //!< The service used to access user's data
@property (strong, nonatomic, readonly) XEEEmailAddressService *emailAddressService; //!< The service used to access a user's email addresses
@property (strong, nonatomic, readonly) XEEPhoneNumberService *phoneNumberService; //!< The service used to access a user's phone numbers
@property (strong, nonatomic, readonly) XEEDataService *dataService; //!< The service used to access the data coming from the device
@property (strong, nonatomic, readonly) XEECarService *carService; //!< The service used to access a user's cars
@property (weak, nonatomic) id<XEEControllerDelegate> delegate;
@property (strong, nonatomic, readonly) NSString *accessToken; //!< The current access token granted by Xee's OAuth server. \note __DO NOT__ store this value. The access token will be renewed automatically by the SDK and therefore its value will change over time.
@property (readonly, nonatomic) BOOL authenticated; //!< YES if the XEEController has successfully authenticated the user - NO otherwise
@property (readonly, nonatomic) BOOL pairedWithDevice; //!< YES if the XEEController is currently paired with the Xee Device - NO otherwise
@property (assign, nonatomic) BOOL useSimulationMode; //!< set this to YES if you want to use the simulation mode instead of trying to pair with the device via bluetooth
@property (assign, nonatomic) BOOL useDevelopmentEnvironment; //!< set this to YES to connect to Xee's development servers


@property (strong, nonatomic) NSString *boxSerialNumber;

+ (XEEController *)sharedInstance;

- (void)authenticateUser; //!< starts the user authentication process

- (void)disconnectUser; //!< disconnects the authenticated user

- (BOOL)updateDeviceFirmware; //!< starts the device firmware update process

- (void)connectToDevice; //!< starts the bluetooth pairing with de device

- (void)disconnectFromDevice; //!< disconnects the XEEController from the device

@end

